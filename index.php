<?php
@session_start();
include_once "administration/config.php";
?>
<!doctype>
	<html>
		<head>
			<title>Esthetique Tatiana</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta name=EsthetiqueTatiana content=”Site du cabinet Esthetique Tatiana.” />
			<meta name=”keywords” content=”esthetique, tatiana, saint-saturnin, soins, produits, estheticienne, lemans” />
			<link rel="stylesheet" type="text/css" href="style.css" />
		</head>

		<body>
			<header>
				<div class="header_center">
					<a href="index.php"><div id="logo"></div></a>

					<?php include('inc/coordonnees.php'); ?>

				</div>
			</header>

			<div class="principal">
				<div id="esth_salon"></div>

				<?php include('inc/aside.php'); ?>

				<section>
					<div id="box_section">
						<div id="titre_box_section">Bienvenue !</div>
							<div class="contenu_box_section">
								<img src="images/pres_1.jpg" style="float: left; height: 270px; width: auto; box-shadow: 0 0 10px"/><img src="images/pres_3.jpg" style="float: right; height: 270px; width: auto; box-shadow: 0 0 10px;"/>
								<img src="images/pres_4.jpg" style="float: left; height: 272px; width: auto; box-shadow: 0 0 10px; margin-top: 10px;"/><img src="images/pres_2.jpg" style="float: right; height: 272px; width: 406px; box-shadow: 0 0 10px; margin-top: 10px;"/>
								
							</div>
					</div>
				</section>

			</div>

			<?php include('inc/footer.php'); ?>
		</body>
	</html>