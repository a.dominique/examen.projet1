<?php
@session_start();
include_once "administration/config.php";
?>

<?php

$email_dest = "esth.tatiana@gmail.com";

if (isset($_POST['envoi'])) {

// E-mail headers:
$headers ="MIME-Version: 1.0 \n";
$headers .="From: Contact<alexis.dominique@bts-malraux.net>\n";


$headers .="Content-Type: text/html; charset=iso-8859-1 \n";

$subject = "alexis.dominique@bts-malraux.net";

$partie_entete = "<html><head>
<meta http-equiv=Content-Type content=text/html; charset=iso-8859-1>
</head>
<body bgcolor=#FFFFFF>";

for ($a=1; $a<= $_POST['nbre_champs_texte']; $a++) {
$partie_champs_texte .= "<font face='Verdana' size='2' color='#003366'>" . $_POST['titre_champ'.$a] . " = " . $_POST['champ'.$a] . "</font><br>";
}

if ($_POST['nbre_zone_email'] != 0) {
$partie_zone_email = "<font face='Verdana' size='2' color='#003366'>" . $_POST['titre_email'] . " = " . $_POST['zone_email'] . "</font><br>";
}

if ($_POST['nbre_zones_texte'] != 0) {
$partie_zone_texte = "<font face='Verdana' size='2' color='#003366'>" . $_POST['titre_zone'] . " = " . $_POST['zone_texte'] . "</font><br>";
$partie_zone_texte = stripslashes($partie_zone_texte);
}

$fin = "</body></html>";

$sortie = $partie_entete . $partie_champs_texte . $partie_zone_email . $partie_zone_texte . $fin;

// Send the e-mail
if (@!mail($email_dest,$subject,$sortie,$headers)) {
echo("Envoi du formulaire impossible");
} else { // Closing if !mail...

// Renvoi à la page de remerciement
header("Location:contact_post.php");
exit();

} // Fin du else
} // Closing if edit
?>

<!doctype>
	<html>
		<head>
			<title>Esthetique Tatiana</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta name=EsthetiqueTatiana content=”Site du cabinet Esthetique Tatiana.” />
			<meta name=”keywords” content=”esthetique, tatiana, saint-saturnin, soins, produits, estheticienne, lemans” />
			<link rel="stylesheet" type="text/css" href="style.css" />

<script language="JavaScript">function verifSelection() {if (mail_form.champ1.value == "") {
alert("Veuillez remplir le champ "Prénom"")
return false
} if (mail_form.champ2.value == "") {
alert("Veuillez remplir le champ "Nom"")
return false
} if (mail_form.champ3.value == "") {
alert("Veuillez remplir le champ "Sexe"")
return false
} if (mail_form.zone_email.value == "") {
alert("Veuillez remplir le champ "Adresse e-mail"")
return false
}

invalidChars = " /:,;'"

for (i=0; i<invalidChars.length; i++) {	// does it contain any invalid characters?
badChar = invalidChars.charAt(i)

if (mail_form.zone_email.value.indexOf(badChar,0) > -1) {
alert("Votre adresse e-mail contient des caractères invalides. Veuillez vérifier.")
mail_form.zone_email.focus()
return false
}
}

atPos = mail_form.zone_email.value.indexOf("@",1)			// there must be one "@" symbol
if (atPos == -1) {
alert('Votre adresse e-mail ne contient pas le signe "@". Veuillez vérifier.')
mail_form.zone_email.focus()
return false
}

if (mail_form.zone_email.value.indexOf("@",atPos+1) != -1) {	// and only one "@" symbol
alert('Il ne doit y avoir qu\'un signe "@". Veuillez vérifier.')
mail_form.zone_email.focus()
return false
}

periodPos = mail_form.zone_email.value.indexOf(".",atPos)

if (periodPos == -1) {					// and at least one "." after the "@"
alert('Vous avez oublié le point "." après le signe "@". Veuillez vérifier.')
mail_form.zone_email.focus()
return false
}

if (periodPos+3 > mail_form.zone_email.value.length)	{		// must be at least 2 characters after the 
alert('Il doit y avoir au moins deux caractères après le signe ".". Veuillez vérifier.')
mail_form.zone_email.focus()
return false
}if (mail_form.zone_texte.value == "") {
alert("Veuillez remplir le champ "Sujet"")
return false
} } // Fin de la fonction

</script>

		</head>

		<body>
			<header>
				<div class="header_center">
					<a href="index.php"><div id="logo"></div></a>

					<?php include('inc/coordonnees.php'); ?>

				</div>
			</header>

			<div class="principal">
				<div id="esth_salon"></div>

				<?php include('inc/aside.php'); ?>

				<section>
					<div id="box_section">
						<div id="titre_box_section">Contact</div>
							<div class="contenu_box_section">

<form name="mail_form" method="post" action="<?=$_SERVER['PHP_SELF']?>" onSubmit="return verifSelection()">
  <div align="center"></div>
<p align="center">
<table width="566" border="0" align="center">
<p align="center">
</p><tr>
      <td><font face="Verdana" size="2">Prénom *</font></td>
      <td><input name="champ1" type="text" id="form_text"></td>
    </tr><tr>
      <td><font face="Verdana" size="2">Nom *</font></td>
      <td><input name="champ2" type="text" id="form_text"></td>
    </tr><tr>
      <td><font face="Verdana" size="2">*</font></td>
      <td>
      <SELECT name="champ3" size="1" id="form_text">
      	<option>...</option>
		<option>Homme</option>
		<option>Femme</option>
	  </SELECT>
</td>
    </tr><tr>
      <td width><font face="Verdana" size="2">Adresse e-mail *</font></td>
      <td width><input name="zone_email" type="text" id="form_text"></td>
    </tr><tr>
      <td valign="top"><font face="Verdana" size="2">Sujet *</font></td>
      <td><textarea name="zone_texte" cols="50" rows="10"></textarea></td>
    </tr><tr>
      <td valign="top"><input name="nbre_champs_texte" type="hidden" id="nbre_champs_texte" value="3">
        <input name="nbre_zones_texte" type="hidden" value="1">
<input name="nbre_zone_email" type="hidden" value="1">
<input name="titre_champ1" type="hidden" value="Prénom"><input name="titre_champ2" type="hidden" value="Nom"><input name="titre_champ3" type="hidden" value="Sexe"><input name="titre_email" type="hidden" value="adresse@adresse.fr"><input name="titre_zone" type="hidden" value="Sujet"></td>
      <td><div align="center">
     	<input type="reset" name="Reset" value="Effacer" id="effacer" style="width: 100%;">          
		<input type="submit" name="envoi" value="Envoyer" id="connexion" style="width: 100%;">
        </div></td>
    </tr>
  </table>
  <div align="center"></div>
</form>
<p style="font-size: 11px; font-style: italic;">* = Champ obligatoire</p>
							</div>
					</div>
				</section>

			</div>

			<?php include('inc/footer.php'); ?>
		</body>
	</html>