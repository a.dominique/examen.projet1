<?php
@session_start();
include('administration/config.php');
?>
<!doctype>
	<html>
		<head>
			<title>Esthetique Tatiana</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta name=EsthetiqueTatiana content=”Site du cabinet Esthetique Tatiana.” />
			<meta name=”keywords” content=”esthetique, tatiana, saint-saturnin, soins, produits, estheticienne, lemans” />
			<link rel="stylesheet" type="text/css" href="style.css" />
		</head>

		<body>
			<header>
				<div class="header_center">
					<a href="index.php"><div id="logo"></div></a>

					<?php include('inc/coordonnees.php'); ?>

				</div>
			</header>

			<div class="principal">
				<div id="esth_salon"></div>

				<?php include('inc/aside.php'); ?>

				<section>
					<div id="box_section">
						<div id="titre_box_section">Administration</div>
							<div class="contenu_box_section">
								<?php include('administration/panel.php'); ?>
					</div>
				</section>

			</div>

			<?php include('inc/footer.php'); ?>
		</body>
	</html>