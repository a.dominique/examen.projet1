﻿<?php
@session_start();
include_once "administration/config.php";
?>
<!doctype>
	<html>
		<head>
			<title>Esthetique Tatiana</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta name=EsthetiqueTatiana content=”Site du cabinet Esthetique Tatiana.” />
			<meta name=”keywords” content=”esthetique, tatiana, saint-saturnin, soins, produits, estheticienne, lemans” />
			<link rel="stylesheet" type="text/css" href="style.css" />
		</head>

		<body>
			<header>
				<div class="header_center">
					<a href="index.php"><div id="logo"></div></a>

					<?php include('inc/coordonnees.php'); ?>

				</div>
			</header>

			<div class="principal">
				<div id="esth_salon"></div>

				<?php include('inc/aside.php'); ?>

				<section>
					<div id="box_section">
						<div id="titre_box_section">Prestations du Cabinet Esthetique Tatiana</div>
							<div class="contenu_box_section" style="font-size: 14px;">
								
								<div id="titre_box_prestations">Epilation douce</div><br>
								<font style="text-transform: uppercase;">Sourcil ou création sourcil</font><i style="float: right;">9,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Lèvres</font> <i style="float: right;">7,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Menton</font> <i style="float: right;">7,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">joues</font> <i style="float: right;">7,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Demi-jambes</font> <i style="float: right;">19,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Cuisses</font> <i style="float: right;">21,00€</i><br>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">3/4 jambes</font> <i style="float: right;">23,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Jambes entières</font> <i style="float: right;">26,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Aisselles</font> <i style="float: right;">10,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Maillot classique</font> <i style="float: right;">15,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Maillot brésilien</font> <i style="float: right;">18,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Maillot intégral</font> <i style="float: right;">23,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Bras</font> <i style="float: right;">19,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Torse</font> <i style="float: right;">22,00€</i><br/>
								<div class="hr_prestations"></div>
 								<font style="text-transform: uppercase;">Dos</font> <i style="float: right;">22,00€</i><br/><br/><br/>

  
 								<div id="titre_box_prestations">Maquillage</div><br/>
 								<font style="text-transform: uppercase;">Jour</font><i style="float: right;">22,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Mariée + essai</font><i style="float: right;">37,00€</i><br/><br/><br/>

								<div id="titre_box_prestations">Mains</div><br/>
								<font style="text-transform: uppercase;">Soin complet des mains</font><i style="float: right;">33,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Pose de vernis</font><i style="float: right;">9,00€</i><br/><br/><br/>

								<div id="titre_box_prestations">Soins du visage THALION</div><br/>
								<font style="text-transform: uppercase;">Douceur infinie (Peaux sensibles)</font><i style="float: right;">54,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Hydratation Absolue (Peaux déshydratées)</font><i style="float: right;">54,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Pureté extrême (Peaux grasses et ternes)</font><i style="float: right;">54,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Nutrition intense (Peaux sèches et déshydratées) </font><i style="float: right;">54,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soin éclat (Nettoyage de peau)</font><i style="float: right;">41,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soin éclat adolescent</font><i style="float: right;">32,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Expert éclat oxygène</font><i style="float: right;">65,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Expert anti-âge </font><i style="float: right;">66,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Instant fermeté  </font><i style="float: right;">66,00€</i><br/><br/><br/>

								<div id="titre_box_prestations">Soins du visage SOSKIN</div><br/>
								<font style="text-transform: uppercase;">Rituel Hydratation (Peaux déshydratées)</font><i style="float: right;">52,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Rituel purifiant (Peaux grasses et ternes)</font><i style="float: right;">52,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Rituel parfait éclat (Peau ternes et asphyxiées)</font><i style="float: right;">62,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Rituel antiride </font><i style="float: right;">63,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soins éclat (Nettoyage de peau) </font><i style="float: right;">41,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soin éclat adolescent</font><i style="float: right;">32,00€</i><br/><br/><br/>


								<div id="titre_box_prestations">Soins du visage SOSKIN (SPM)</div><br/>
								<font style="text-transform: uppercase;">Soin antiride parfait</font><i style="float: right;">30,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soin antiride fermeté</font><i style="float: right;">30,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soin purifiant essentiel</font><i style="float: right;">30,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soin clarifiant expert</font><i style="float: right;">30,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Cure de 3 soins</font><i style="float: right;">70,00€</i><br/><br/><br/>

								<div id="titre_box_prestations">Soins du corps</div><br/>
								<font style="text-transform: uppercase;">Modelage relaxant</font><i style="float: right;">52,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Modelage dos</font><i style="float: right;">32,00€</i><br/>
								<div class="hr_prestations"></div>
								<font style="text-transform: uppercase;">Soin du dos spécial relax <i>(gommage, boue-marine et modelage)</i></font><i style="float: right;">62,00€</i><br/>

							</div>
					</div>
				</section>

			</div>

			<?php include('inc/footer.php'); ?>
		</body>
	</html>