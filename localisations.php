<?php
@session_start();
include_once "administration/config.php";
?>
<!doctype>
	<html>
		<head>
			<title>Esthetique Tatiana</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta name=EsthetiqueTatiana content=”Site du cabinet Esthetique Tatiana.” />
			<meta name=”keywords” content=”esthetique, tatiana, saint-saturnin, soins, produits, estheticienne, lemans” />
			<link rel="stylesheet" type="text/css" href="style.css" />
		</head>

		<body>
			<header>
				<div class="header_center">
					<a href="index.php"><div id="logo"></div></a>

					<?php include('inc/coordonnees.php'); ?>

				</div>
			</header>

			<div class="principal">
				<div id="esth_salon"></div>

				<?php include('inc/aside.php'); ?>

				<section>
					<div id="box_section">
						<div id="titre_box_section">Geolocalisation du Cabinet Esthetique Tatiana</div>
							<div class="contenu_box_section">
								
							<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
							<script type="text/javascript"> 
							function initCarte(){ 
							// création de la carte 
							var oMap = new google.maps.Map( document.getElementById( 'div_carte'),{ 
							'center' : new google.maps.LatLng( 48.0622908, 0.1641292), 
							'zoom' : 130, 
							'mapTypeId' : google.maps.MapTypeId.ROADMAP 
							}); 
							} 
							// init lorsque la page est chargée 
							google.maps.event.addDomListener( window, 'load', initCarte); 
							</script>

							<div id="div_carte"></div>
								
							</div>
					</div>
				</section>

			</div>

			<?php include('inc/footer.php'); ?>
		</body>
	</html>