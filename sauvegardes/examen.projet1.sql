--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

-- Started on 2018-04-18 17:17:42 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 12 (class 2615 OID 31896)
-- Name: examen.projet1; Type: SCHEMA; Schema: -; Owner: a.dominique
--

CREATE SCHEMA "examen.projet1";


ALTER SCHEMA "examen.projet1" OWNER TO "a.dominique";

--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 12
-- Name: SCHEMA "examen.projet1"; Type: COMMENT; Schema: -; Owner: a.dominique
--

COMMENT ON SCHEMA "examen.projet1" IS 'Schéma spécialisé pour le passage de l''épreuve E4 de l''examen du BTS SIO 2018';


SET search_path = "examen.projet1", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 211 (class 1259 OID 32911)
-- Name: 1b6d5fb8846f2f0d29fa61dd7eb33139; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE "1b6d5fb8846f2f0d29fa61dd7eb33139" (
    id integer NOT NULL,
    date_achat date NOT NULL,
    id_produit character varying(50) NOT NULL,
    quantite_produit character varying(50) NOT NULL,
    rendez_vous character varying(50) NOT NULL
);


ALTER TABLE "1b6d5fb8846f2f0d29fa61dd7eb33139" OWNER TO "a.dominique";

--
-- TOC entry 210 (class 1259 OID 32909)
-- Name: 1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE "1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq" OWNER TO "a.dominique";

--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 210
-- Name: 1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE "1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq" OWNED BY "1b6d5fb8846f2f0d29fa61dd7eb33139".id;


--
-- TOC entry 213 (class 1259 OID 32917)
-- Name: 204d325dff9f548e7b9fcc897eb9c62a; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE "204d325dff9f548e7b9fcc897eb9c62a" (
    id integer NOT NULL,
    date_achat date NOT NULL,
    id_produit character varying(50) NOT NULL,
    quantite_produit character varying(50) NOT NULL,
    rendez_vous character varying(50) NOT NULL
);


ALTER TABLE "204d325dff9f548e7b9fcc897eb9c62a" OWNER TO "a.dominique";

--
-- TOC entry 212 (class 1259 OID 32915)
-- Name: 204d325dff9f548e7b9fcc897eb9c62a_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE "204d325dff9f548e7b9fcc897eb9c62a_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "204d325dff9f548e7b9fcc897eb9c62a_id_seq" OWNER TO "a.dominique";

--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 212
-- Name: 204d325dff9f548e7b9fcc897eb9c62a_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE "204d325dff9f548e7b9fcc897eb9c62a_id_seq" OWNED BY "204d325dff9f548e7b9fcc897eb9c62a".id;


--
-- TOC entry 215 (class 1259 OID 32923)
-- Name: a6c5db6d92788ffea1da09cf7979e73d; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE a6c5db6d92788ffea1da09cf7979e73d (
    id integer NOT NULL,
    date_achat date NOT NULL,
    id_produit character varying(50) NOT NULL,
    quantite_produit character varying(50) NOT NULL,
    rendez_vous character varying(50) NOT NULL
);


ALTER TABLE a6c5db6d92788ffea1da09cf7979e73d OWNER TO "a.dominique";

--
-- TOC entry 214 (class 1259 OID 32921)
-- Name: a6c5db6d92788ffea1da09cf7979e73d_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE a6c5db6d92788ffea1da09cf7979e73d_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE a6c5db6d92788ffea1da09cf7979e73d_id_seq OWNER TO "a.dominique";

--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 214
-- Name: a6c5db6d92788ffea1da09cf7979e73d_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE a6c5db6d92788ffea1da09cf7979e73d_id_seq OWNED BY a6c5db6d92788ffea1da09cf7979e73d.id;


--
-- TOC entry 209 (class 1259 OID 31941)
-- Name: clients; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE clients (
    id integer NOT NULL,
    prenom text NOT NULL,
    nom text NOT NULL,
    naissance date NOT NULL,
    email text NOT NULL,
    telephone text NOT NULL,
    produit smallint,
    achat text,
    cle character varying(50) NOT NULL
);


ALTER TABLE clients OWNER TO "a.dominique";

--
-- TOC entry 208 (class 1259 OID 31939)
-- Name: clients_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE clients_id_seq OWNER TO "a.dominique";

--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 208
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- TOC entry 200 (class 1259 OID 31897)
-- Name: inventaire; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE inventaire (
    id integer NOT NULL,
    marque text NOT NULL,
    produit text NOT NULL,
    quantite smallint NOT NULL,
    prix real NOT NULL,
    total real NOT NULL
);


ALTER TABLE inventaire OWNER TO "a.dominique";

--
-- TOC entry 201 (class 1259 OID 31903)
-- Name: inventaire_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE inventaire_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventaire_id_seq OWNER TO "a.dominique";

--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 201
-- Name: inventaire_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE inventaire_id_seq OWNED BY inventaire.id;


--
-- TOC entry 202 (class 1259 OID 31905)
-- Name: maintenance; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE maintenance (
    id integer NOT NULL,
    etat integer NOT NULL
);


ALTER TABLE maintenance OWNER TO "a.dominique";

--
-- TOC entry 203 (class 1259 OID 31908)
-- Name: maintenance_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE maintenance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE maintenance_id_seq OWNER TO "a.dominique";

--
-- TOC entry 2247 (class 0 OID 0)
-- Dependencies: 203
-- Name: maintenance_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE maintenance_id_seq OWNED BY maintenance.id;


--
-- TOC entry 204 (class 1259 OID 31910)
-- Name: page; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE page (
    id integer NOT NULL,
    titre text NOT NULL,
    contenu text NOT NULL,
    page text NOT NULL,
    heure character varying(250) NOT NULL,
    pseudo character varying(30) NOT NULL
);


ALTER TABLE page OWNER TO "a.dominique";

--
-- TOC entry 205 (class 1259 OID 31916)
-- Name: page_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE page_id_seq OWNER TO "a.dominique";

--
-- TOC entry 2248 (class 0 OID 0)
-- Dependencies: 205
-- Name: page_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE page_id_seq OWNED BY page.id;


--
-- TOC entry 206 (class 1259 OID 31918)
-- Name: users; Type: TABLE; Schema: examen.projet1; Owner: a.dominique
--

CREATE TABLE users (
    id integer NOT NULL,
    ip character varying(250) NOT NULL,
    pseudo character varying(30) NOT NULL,
    nom text NOT NULL,
    mdp text NOT NULL,
    rang integer NOT NULL,
    cle character varying(60) NOT NULL,
    actif smallint NOT NULL,
    email text NOT NULL
);


ALTER TABLE users OWNER TO "a.dominique";

--
-- TOC entry 207 (class 1259 OID 31924)
-- Name: users_id_seq; Type: SEQUENCE; Schema: examen.projet1; Owner: a.dominique
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO "a.dominique";

--
-- TOC entry 2249 (class 0 OID 0)
-- Dependencies: 207
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: examen.projet1; Owner: a.dominique
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 2091 (class 2604 OID 32914)
-- Name: 1b6d5fb8846f2f0d29fa61dd7eb33139 id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY "1b6d5fb8846f2f0d29fa61dd7eb33139" ALTER COLUMN id SET DEFAULT nextval('"1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq"'::regclass);


--
-- TOC entry 2092 (class 2604 OID 32920)
-- Name: 204d325dff9f548e7b9fcc897eb9c62a id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY "204d325dff9f548e7b9fcc897eb9c62a" ALTER COLUMN id SET DEFAULT nextval('"204d325dff9f548e7b9fcc897eb9c62a_id_seq"'::regclass);


--
-- TOC entry 2093 (class 2604 OID 32926)
-- Name: a6c5db6d92788ffea1da09cf7979e73d id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY a6c5db6d92788ffea1da09cf7979e73d ALTER COLUMN id SET DEFAULT nextval('a6c5db6d92788ffea1da09cf7979e73d_id_seq'::regclass);


--
-- TOC entry 2090 (class 2604 OID 31944)
-- Name: clients id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- TOC entry 2086 (class 2604 OID 31926)
-- Name: inventaire id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY inventaire ALTER COLUMN id SET DEFAULT nextval('inventaire_id_seq'::regclass);


--
-- TOC entry 2087 (class 2604 OID 31927)
-- Name: maintenance id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY maintenance ALTER COLUMN id SET DEFAULT nextval('maintenance_id_seq'::regclass);


--
-- TOC entry 2088 (class 2604 OID 31928)
-- Name: page id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY page ALTER COLUMN id SET DEFAULT nextval('page_id_seq'::regclass);


--
-- TOC entry 2089 (class 2604 OID 31929)
-- Name: users id; Type: DEFAULT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2232 (class 0 OID 32911)
-- Dependencies: 211
-- Data for Name: 1b6d5fb8846f2f0d29fa61dd7eb33139; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--

INSERT INTO "1b6d5fb8846f2f0d29fa61dd7eb33139" VALUES (1, '2018-04-17', '15', '5', 'Soin du visage');


--
-- TOC entry 2250 (class 0 OID 0)
-- Dependencies: 210
-- Name: 1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('"1b6d5fb8846f2f0d29fa61dd7eb33139_id_seq"', 1, true);


--
-- TOC entry 2234 (class 0 OID 32917)
-- Dependencies: 213
-- Data for Name: 204d325dff9f548e7b9fcc897eb9c62a; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--

INSERT INTO "204d325dff9f548e7b9fcc897eb9c62a" VALUES (2, '2018-04-17', '10', '5', 'Soin du visage');


--
-- TOC entry 2251 (class 0 OID 0)
-- Dependencies: 212
-- Name: 204d325dff9f548e7b9fcc897eb9c62a_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('"204d325dff9f548e7b9fcc897eb9c62a_id_seq"', 2, true);


--
-- TOC entry 2236 (class 0 OID 32923)
-- Dependencies: 215
-- Data for Name: a6c5db6d92788ffea1da09cf7979e73d; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--

INSERT INTO a6c5db6d92788ffea1da09cf7979e73d VALUES (1, '2018-04-17', '12', '5', 'Soin du visage');


--
-- TOC entry 2252 (class 0 OID 0)
-- Dependencies: 214
-- Name: a6c5db6d92788ffea1da09cf7979e73d_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('a6c5db6d92788ffea1da09cf7979e73d_id_seq', 1, true);


--
-- TOC entry 2230 (class 0 OID 31941)
-- Dependencies: 209
-- Data for Name: clients; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--

INSERT INTO clients VALUES (15, 'Amael', 'DURIX', '1997-11-10', 'amael.durix@wanadoo.fr', '0689565689', 6, NULL, '204d325dff9f548e7b9fcc897eb9c62a');
INSERT INTO clients VALUES (16, 'Thibaud', 'FONTAINE', '1999-04-22', 'thibaud.fontaine@gmail.com', '0678454578', 5, NULL, 'a6c5db6d92788ffea1da09cf7979e73d');
INSERT INTO clients VALUES (14, 'Brice', 'JACQUELIN', '1997-10-12', 'brice.jacquelin@gmail.com', '0656235623', 5, NULL, '1b6d5fb8846f2f0d29fa61dd7eb33139');


--
-- TOC entry 2253 (class 0 OID 0)
-- Dependencies: 208
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('clients_id_seq', 16, true);


--
-- TOC entry 2221 (class 0 OID 31897)
-- Dependencies: 200
-- Data for Name: inventaire; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--

INSERT INTO inventaire VALUES (13, 'MARQUEALEATOIRE', 'Produit peau 3', 25, 4, 100);
INSERT INTO inventaire VALUES (14, 'MARQUEALEATOIRE', 'Produit peau 4', 10, 5, 50);
INSERT INTO inventaire VALUES (16, 'MARQUEALEATOIRE', 'Produit peau 6', 10, 1, 10);
INSERT INTO inventaire VALUES (18, 'MARQUEALEATOIRE', 'Produit peau 8', 10, 5, 50);
INSERT INTO inventaire VALUES (17, 'MARQUEALEATOIRE', 'Produit peau 7', 5, 20, 100);
INSERT INTO inventaire VALUES (1, 'AUCUN', 'AUCUN', 10001, 0, 0);
INSERT INTO inventaire VALUES (10, 'MARQUEALEATOIRE', 'Produit peau', 50, 3, 150);
INSERT INTO inventaire VALUES (12, 'MARQUEALEATOIRE', 'Produit peau 2', 5, 5, 25);
INSERT INTO inventaire VALUES (15, 'MARQUEALEATOIRE', 'Produit peau 5', 15, 10, 150);


--
-- TOC entry 2254 (class 0 OID 0)
-- Dependencies: 201
-- Name: inventaire_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('inventaire_id_seq', 9, true);


--
-- TOC entry 2223 (class 0 OID 31905)
-- Dependencies: 202
-- Data for Name: maintenance; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--



--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 203
-- Name: maintenance_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('maintenance_id_seq', 1, false);


--
-- TOC entry 2225 (class 0 OID 31910)
-- Dependencies: 204
-- Data for Name: page; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--



--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 205
-- Name: page_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('page_id_seq', 1, false);


--
-- TOC entry 2227 (class 0 OID 31918)
-- Dependencies: 206
-- Data for Name: users; Type: TABLE DATA; Schema: examen.projet1; Owner: a.dominique
--

INSERT INTO users VALUES (1, '80.82.238.198', 'Grégory', 'DAVID', 'd033e22ae348aeb5660fc2140aec35850c4da997', 2, '1', 1, 'gregory.david@bts-malraux.net');
INSERT INTO users VALUES (2, '80.82.238.198', 'Géraldine', 'TAYSSE', 'd033e22ae348aeb5660fc2140aec35850c4da997', 2, '1', 1, 'geraldine.taysse@bts-malraux.net');


--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 207
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: examen.projet1; Owner: a.dominique
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- TOC entry 2103 (class 2606 OID 31949)
-- Name: clients clients_pkey; Type: CONSTRAINT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- TOC entry 2095 (class 2606 OID 31931)
-- Name: inventaire inventaire_pkey; Type: CONSTRAINT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY inventaire
    ADD CONSTRAINT inventaire_pkey PRIMARY KEY (id);


--
-- TOC entry 2097 (class 2606 OID 31933)
-- Name: maintenance maintenance_pkey; Type: CONSTRAINT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY maintenance
    ADD CONSTRAINT maintenance_pkey PRIMARY KEY (id);


--
-- TOC entry 2099 (class 2606 OID 31935)
-- Name: page page_pkey; Type: CONSTRAINT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);


--
-- TOC entry 2101 (class 2606 OID 31937)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: examen.projet1; Owner: a.dominique
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


-- Completed on 2018-04-18 17:17:43 CEST

--
-- PostgreSQL database dump complete
--

